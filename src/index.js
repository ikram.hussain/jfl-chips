import React from "react";
import { render } from "react-dom";
import { Configuration } from "@react-md/layout";
import ErrorBoundary from './ErrorBoundary'
import AutoCommplate from "./AutoCommplate";
import "./InputChips.scss";

const App = () => (
  <Configuration>
    <ErrorBoundary>
    <AutoCommplate />
    </ErrorBoundary>
  </Configuration>
);

render(<App />, document.getElementById("root"));
