import React from "react";
import AutoComplateView from "./AutoCompleteView";
import "./InputChips.scss";
import { Avatar } from "@react-md/avatar";

import { debounce } from "throttle-debounce";

class AutoComplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chips: [],
      moviesArray: []
    };
  }
  onAutoComplete = result => {
    const { id } = result.result;
    let filterValue = this.state.moviesArray.find(obj => obj.id === id);
    if (!filterValue) {
      throw new Error();
    }

    this.setState({ chips: [...this.state.chips, filterValue] });
  };
  deleteChips(id) {
    this.setState({ chips: this.state.chips.filter(chip => chip.id !== id) });
  }

  searchHanlder(e) {
    e.persist();
    debounce(500, () => {
      this.getMovies(e.target.value);
    })();
  }
  debouncedGetMovie = debounce(query => {
    this.getMovies(query);
  }, 500);

  async getMovies(title) {
    let url = "http://www.omdbapi.com/?s=" + title + "&apikey=6f0b9057";
    let response = await fetch(url);
    let movies = await response.json();
    console.log(movies);
    if (movies.Response === "True") {
      const transformedArray = movies.Search.map(
        ({ Title, imdbID, Year, Poster }) => ({
          label: Title,
          leftAvatar: <Avatar src={Poster} />,
          poster: Poster,
          id: imdbID,
          secondaryText: "Year- " + Year
        })
      );
      console.log(transformedArray);
      this.setState({ moviesArray: transformedArray });
    }
  }

  render() {
    return (
      <AutoComplateView
        onAutoComplete={this.onAutoComplete.bind(this)}
        chips={this.state.chips}
        searchHanlder={this.searchHanlder.bind(this)}
        deleteChips={this.deleteChips.bind(this)}
        data={this.state.moviesArray}
      />
    );
  }
}

export default AutoComplate;
