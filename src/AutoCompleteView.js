import React from "react";
import { AutoComplete } from "@react-md/autocomplete";
const AutoCompleteView = ({data,chips,searchHanlder,onAutoComplete,deleteChips}) => (
 
    <div className="setmargin">
      <p>You Can  Select Only 5 Movies</p>
      <p>Data Searching based on minimum 3 charecters</p>
      <div className="border">
        {chips.map(({ id, label,poster }) => (
          <div className="chip" key={id}>
            {poster!=='N/A'?
             <img src={poster} alt="Person" width="96" height="96"/>
        :<span className="circel">{label.charAt(0).toUpperCase()}</span>}
            {label}
            <span
              className="closebtn"
              onClick={(e) =>deleteChips(id)}
             
              
            >
              &times;
            </span>
          </div>
        ))}
        <AutoComplete
          id="input-chips-email"
          placeholder="Search Movie"
          theme="none"
          valueKey="label"
          data={data.filter(({ id }) => !chips.find(chip => chip.id === id))}
          listboxWidth="auto"
          onChange={e => searchHanlder(e)}
          inline
          highlight
          autoComplete={chips.length<5?"both":'none'}
          max="5"
          clearOnAutoComplete
          onAutoComplete={onAutoComplete}
        />
      </div>
    </div>
  );


export default AutoCompleteView;
